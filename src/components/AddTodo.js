import React, { Component } from "react";

export class AddTodo extends Component {

  state = {
    title: ''
  }


  onChange = (e) => this.setState({title: e.target.value});

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState( { title: ''});
  }
  render() {
    return (
      <div className="container-fluid">
        <form onSubmit={this.onSubmit} style={formStyle}>
        <input 
          type="text" 
          name="title" 
          placeholder="Ajouter une note" 
          style={addTodoStyle}
          value={this.state.title}
          onChange={this.onChange}

        />
        <input
          type="submit" 
          value="Valider" 
          style={buttonStyle}
/>
        </form>
      </div>
    );
  }
}

const addTodoStyle = {
  flex: '10'

}


const buttonStyle = {
  flex: '1'
}

const formStyle = {
  display: 'flex'
}
export default AddTodo;
