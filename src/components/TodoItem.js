import React, { Component } from "react";
import PropTypes from "prop-types";

export class TodoItem extends Component {
  getStyle = () => {
    return {
      background: "#f4f4f4",
      padding: "10px",
      textAlign: "center",
      borderBottom: "1px #ccc dotted",
      textDecoration: this.props.todo.completed ? "line-through" : "none"
    };
  };

  render() {
    return (
      <div className="container" style={this.getStyle()}>
        <p>
          <input
            type="checkBox"
            onChange={this.props.checkDone.bind(this, this.props.todo.id)}
            style={checkBoxStyle}
          />
          {this.props.todo.title}
          <button
            type="button"
            aria-label="Close"
            style={delButtonStyle}
            onClick={this.props.delTodo.bind(this, this.props.todo.id)}
          >
            x
          </button>
        </p>
      </div>
    );
  }
}

const checkBoxStyle = {
  paddingRight: "10px",
  float: "left"
};

const delButtonStyle = {
  float: "right"
};

TodoItem.propTypes = {
  todo: PropTypes.object.isRequired
};

export default TodoItem;
