import React, { Component } from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';


class Todos extends Component {


  todoStyle = () => {




  }
  render() {
    return this.props.todos.map((todo) => (

          <TodoItem key={todo.id} todo={todo} checkDone={this.props.checkDone} delTodo={this.props.delTodo}/>


     ));
  }
}

Todos.propTypes = {
  todos: PropTypes.array.isRequired
}

export default Todos;
