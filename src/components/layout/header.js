import React, { Component } from "react";
import { Link } from "react-router-dom";

export class header extends Component {
  render() {
    return (
      <header>
        <div className="container" style={titleStyle}>
          <h3> Agenda React </h3>
          <Link style={styleLien} to="/">Agenda</Link> | <Link style={styleLien} to="/about">&Agrave; propos</Link>
        </div>
      </header>
    );
  }
}

const titleStyle = {
  textAlign: "center",
  background: "#333",
  color: "#fff",
  padding: "10px"
}

const styleLien = {
   color: '#fff',
   textDecoration: 'none'


}
export default header;
