import React from 'react'

export default function About() {
  return (
    <React.Fragment>

        <h1>&Agrave; propos</h1>
        <p>L'application ReactAgenda v1.0.0 fait partie d'une s&eacute;rie d'applications que je d&eacute;veloppe dans mon temps libre
            pour le fun, je pars sur des projets tr&egrave;s basiques que je d&egrave;veloppe par la suite.
        </p>
      
    </React.Fragment>
  )
}
